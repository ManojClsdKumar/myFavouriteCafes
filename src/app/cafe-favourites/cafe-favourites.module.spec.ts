import { CafeFavouritesModule } from './cafe-favourites.module';

describe('CafeFavouritesModule', () => {
  let cafeFavouritesModule: CafeFavouritesModule;

  beforeEach(() => {
    cafeFavouritesModule = new CafeFavouritesModule();
  });

  it('should create an instance', () => {
    expect(cafeFavouritesModule).toBeTruthy();
  });
});
