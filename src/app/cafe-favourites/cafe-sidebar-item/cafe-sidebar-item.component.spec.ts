import { CafelistService, CafeItem } from './../cafelist.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { CafeSidebarItemComponent } from './cafe-sidebar-item.component';
import { DebugElement } from '@angular/core';

describe('CafeSidebarItemComponent', () => {
  let component: CafeSidebarItemComponent;
  let fixture: ComponentFixture<CafeSidebarItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CafeSidebarItemComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(CafeSidebarItemComponent);
    component = fixture.componentInstance;

    let testItem: CafeItem = {
      name: 'testCafe',
      favourite: false,
      location: { 'lat': -35, 'lng': 155 }
    }

    component.cafeObject = testItem;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render correct cafe text in panel', () => {

    expect(fixture.debugElement.nativeElement.querySelector('span')
      .textContent)
      .toContain('testCafe');
  });

  it('should render favourite symbol in panel', () => {

    let testItem: CafeItem = {
      name: 'testCafe',
      favourite: false,
      location: { 'lat': -35, 'lng': 155 }
    }
    component.cafeObject = testItem;

    fixture.detectChanges();
    const DpanelElement: DebugElement = fixture.debugElement;
    const EpanelElement = DpanelElement.query(By.css('.favouriteCafe'))
    const nElement: HTMLElement = EpanelElement.nativeElement;
    expect(nElement.classList[2]).toEqual('fa-star-o')

  });

  it('should render empty favourite symbol in panel', () => {

    let testItemFavourited: CafeItem = {
      name: 'testCafe',
      favourite: true,
      location: { 'lat': -35, 'lng': 155 }
    }
    component.cafeObject = testItemFavourited;

    fixture.detectChanges();
    const DpanelElement: DebugElement = fixture.debugElement;

    const EpanelElement = DpanelElement.query(By.css('.favouriteCafe'))
    const nElement: HTMLElement = EpanelElement.nativeElement;
    expect(nElement.classList[2]).toEqual('fa-star')

  });

  it('should colorFavourited Cafes in panel', () => {

    let testItemFavourited: CafeItem = {
      name: 'testCafe',
      favourite: true,
      location: { 'lat': -35, 'lng': 155 }
    }

    component.cafeObject = testItemFavourited;

    fixture.detectChanges();

    const DpanelElement: DebugElement = fixture.debugElement;
    const EpanelElement = DpanelElement.query(By.css('.cafesidebarItem'))
    const nElement: HTMLElement = EpanelElement.nativeElement;
    expect(nElement.classList[1]).toEqual('favouritedBackgroundColor')
  });

  it('should non-favourited Cafes in panel', () => {

    let testItemFavourited: CafeItem = {
      name: 'testCafe',
      favourite: true,
      location: { 'lat': -35, 'lng': 155 }
    }

    component.cafeObject = testItemFavourited;

    fixture.detectChanges();

    const DpanelElement: DebugElement = fixture.debugElement;
    const EpanelElement = DpanelElement.query(By.css('.cafesidebarItem'))
    const nElement: HTMLElement = EpanelElement.nativeElement;
    expect(nElement.classList[1]).toEqual('favouritedBackgroundColor')
  });

});


