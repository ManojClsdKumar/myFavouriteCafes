import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { CafeItem, CafelistService } from '../cafelist.service';

@Component({
  selector: 'app-cafe-sidebar-item',
  templateUrl: './cafe-sidebar-item.component.html',
  styleUrls: ['./cafe-sidebar-item.component.scss']
})
export class CafeSidebarItemComponent implements OnInit {

  @Input() cafeObject: CafeItem

  constructor(private cafelistService: CafelistService, private changeDetectorRef: ChangeDetectorRef) { }


  makeCafeFavourite(cafeItem) {
    this.cafelistService.toggleFavouriteCafe(cafeItem)
    this.changeDetectorRef.detectChanges();
  }

  ngOnInit() {
  }

}
