import { TestBed, inject } from '@angular/core/testing';
import { CafelistService } from './cafelist.service';

describe('CafelistService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CafelistService]
    });
  });

  it('should be created', inject([CafelistService], (service: CafelistService) => {
    expect(service).toBeTruthy();
  }));

  it('addCafeToList - adds a cafe to the service cafe list', inject([CafelistService], (service: CafelistService) => {
    let cafeItem = {
      name: 'testCafeNonFav',
      favourite: true,
      geometry: {
        location: { 'lat': () => -34, 'lng': () => 154 }
      }
    }

    service.addCafeToList(cafeItem);
    expect(service.localCafeList.length).toEqual(1);

  }));

  it('toggleFavouriteCafe - toggles cafe favourite property', inject([CafelistService], (service: CafelistService) => {
    let cafeItem = {
      name: 'testCafeNonFav',
      favourite: true,
      location: { 'lat': -35, 'lng': 155 }
    }

    service.localCafeList.push(cafeItem);
    service.toggleFavouriteCafe(cafeItem);
    expect(service.localCafeList[0].favourite).toBe(true);

    service.toggleFavouriteCafe(cafeItem);
    expect(service.localCafeList[0].favourite).toBe(false);
  }));

  it('applyFavourites - ensures the ', () => {
    
  });

  it('matchCafes - returns true if two cafes match.', inject([CafelistService], (service: CafelistService) => {


    let testCafeId2 = {
      name: 'testCafeNonFav',
      favourite: true,
      location: { 'lat': -34, 'lng': 154 }
    }

    let testCafeId1 = {
      name: 'testCafeNonFav',
      favourite: false,
      location: { 'lat': -34, 'lng': 154 }
    }

    let matchResult = service.matchCafes(testCafeId1, testCafeId2);
    expect(matchResult).toEqual(true)
  }));

  it('matchCafes - returns false if two cafes do not match.', inject([CafelistService], (service: CafelistService) => {


    let testCafeId2 = {
      name: 'testCafeNonFav',
      favourite: true,
      location: { 'lat': -34, 'lng': 154 }
    }

    let testCafeId1 = {
      name: 'testCafeNonFavDIFFERENT',
      favourite: false,
      location: { 'lat': -34, 'lng': 154 }
    }

    let matchResult = service.matchCafes(testCafeId1, testCafeId2);
    expect(matchResult).toEqual(false)
  }));



});
