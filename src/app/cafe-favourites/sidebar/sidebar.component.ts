import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CafelistService, CafeItem } from '../cafelist.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],

})
export class SidebarComponent implements OnInit {

  localCafeList: CafeItem[] = [];


  constructor(private cafeListService: CafelistService, private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.listenForCafeListChanges();
  }

  listenForCafeListChanges() {
    this.cafeListService.localCafeListSubject.subscribe((cafeList) => {
      this.localCafeList = [];
      this.localCafeList = [...cafeList];
      window.scrollTo(0, 0)
      this.changeDetectorRef.detectChanges()
    })
  }




}
