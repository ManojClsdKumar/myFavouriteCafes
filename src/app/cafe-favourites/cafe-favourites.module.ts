import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavouriteCafesHomeComponent } from './favourite-cafes-home/favourite-cafes-home.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MapComponent } from './map/map.component';

import { CafeSidebarItemComponent } from './cafe-sidebar-item/cafe-sidebar-item.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule
  ],
  declarations: [FavouriteCafesHomeComponent, SidebarComponent, MapComponent, CafeSidebarItemComponent],
  exports: [FavouriteCafesHomeComponent]
})
export class CafeFavouritesModule { }
