
export interface markerConstructor {
    position: any
    map: any
    title: string
    icon: string
}

export class mockMarker {

    constructor(private markerBuildObject: markerConstructor) {

    }

    getPosition() {
        return {
            'lat': () => {
                return this.markerBuildObject.position.lat
            },
            'lng': () => {
                return this.markerBuildObject.position.lng
            }
        }
    }

    getTitle() {
        return this.markerBuildObject.title;
    }

    setIcon(icon: string) {
        this.markerBuildObject.icon = icon;
    }

}
