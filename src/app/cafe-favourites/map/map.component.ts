import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { CafelistService, CafeItem } from '../cafelist.service';
declare const google: any;


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @ViewChild('gmap') mapElement: any;

  cafeMap;
  mapPanDebounceTimer = null;
  isLoading = false;
  initiallyLoaded = false;
  markerArray = [];
  localCafeArray = [];



  constructor(private cafeListService: CafelistService, private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    const mapProp = {
      center: new google.maps.LatLng(-33.865143, 151.209900),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };


    this.cafeMap = new google.maps.Map(this.mapElement.nativeElement, mapProp);
    this.debouncedPanCafeListRefresh();
    this.cafeMap.addListener('center_changed', () => {
      this.debouncedPanCafeListRefresh();
    });


    this.cafeListService.localCafeListSubject.subscribe((cafelist: CafeItem[]) => {

      this.localCafeArray = cafelist;
      this.isLoading = false;
      this.colorFavouriteCafes();
      this.changeDetectorRef.detectChanges();

      if (!this.initiallyLoaded) {
        cafelist.forEach((cafeitem: CafeItem) => {
          this.addMarker(cafeitem)
        })
        this.initiallyLoaded = true;
      }
    })
  }

  colorFavouriteCafes() {
    this.markerArray.forEach((marker) => {

      marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');

      let favouriteMatched = this.localCafeArray.find((cafe: CafeItem) => {
        let cafeLatCheck = cafe.location.lat == marker.getPosition().lat()
        let cafeLngCheck = cafe.location.lng == marker.getPosition().lng()

        if (cafe.name == marker.getTitle() && cafeLatCheck && cafeLngCheck && cafe.favourite == true) {
          return true;
        }
      })

      if (favouriteMatched) {
        marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
      }

      google.maps.event.trigger(this.cafeMap, 'resize');
    });
    return this.markerArray;
  }

  debouncedPanCafeListRefresh() {
    // starts a timer for 2 seconds, when the timer expires cafe loading will start.
    // if the function is called again, and a previous count is in progress...
    // it will reset the count to 2 seconds.
    
    this.isLoading = true;
    this.changeDetectorRef.detectChanges();

    if (this.mapPanDebounceTimer != null) {
      clearTimeout(this.mapPanDebounceTimer);
    }

    this.mapPanDebounceTimer = setTimeout(() => {
      this.clearMarkers();
      this.cafeListService.refreshCafeList(this.cafeMap).then(() => {
        this.localCafeArray.forEach((cafeitem: CafeItem) => {
          this.addMarker(cafeitem)
        })
        this.changeDetectorRef.detectChanges();
      }).catch((err) => {
        console.log(err);
        console.log("Unable to load places for this segment.")
      })
    }, 2000)
  }

  clearMarkers() {
    this.markerArray.forEach((marker) => {
      marker.setMap(null);
    })
  }

  addMarker(cafeitem: CafeItem) {
    let markerColor = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'

    if (this.localCafeArray.find(
      (cafe) => {
        if (this.cafeListService.matchCafes(cafe, cafeitem) && cafeitem.favourite == true) {
          return true
        }
      }) != undefined) {
      markerColor = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    }

    const marker = new google.maps.Marker({
      position: cafeitem.location,
      map: this.cafeMap,
      title: cafeitem.name,
      icon: markerColor
    });


    google.maps.event.addListener(marker, 'click', () => {
      this.cafeListService.toggleFavouriteCafe(cafeitem)
      this.changeDetectorRef.detectChanges();
    })

    this.markerArray.push(marker);

  }


}
