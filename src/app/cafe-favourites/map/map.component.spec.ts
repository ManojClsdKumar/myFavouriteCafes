import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MapComponent } from './map.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CafeItem } from '../cafelist.service'
import { mockMarker } from './marker.mock';


describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;




  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MapComponent],
      imports: [
        MatProgressSpinnerModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);

    let testCafeFav: CafeItem = {
      name: 'testCafeFav',
      favourite: true,
      location: { 'lat': -35, 'lng': 155 }
    }

    let testCafeNonFav: CafeItem = {
      name: 'testCafeNonFav',
      favourite: false,
      location: { 'lat': -34, 'lng': 154 }
    }

    let testCafe3: CafeItem = {
      name: 'testCafeNonFav',
      favourite: true,
      location: { 'lat': -34, 'lng': 154 }
    }

    let marker1 = new mockMarker({
      position: testCafeFav.location,
      map: this.cafeMap,
      title: testCafeFav.name,
      icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
    });

    let marker2 = new mockMarker({
      position: testCafeNonFav.location,
      map: this.cafeMap,
      title: testCafeNonFav.name,
      icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
    });

    let testlocalCafeList = [];
    testlocalCafeList.push(testCafeFav);
    testlocalCafeList.push(testCafeNonFav);

    let testMarkerArray = [];
    testMarkerArray.push(marker1);
    testMarkerArray.push(marker2);


    component = fixture.componentInstance;
    component.localCafeArray = testlocalCafeList;
    component.markerArray = testMarkerArray;
    fixture.detectChanges();
  });




  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should color markers that are favourites', () => {
    component.colorFavouriteCafes();
    expect(component.markerArray[0].markerBuildObject.icon)
      .toEqual('http://maps.google.com/mapfiles/ms/icons/green-dot.png');

    expect(component.markerArray[1].markerBuildObject.icon)
      .toEqual('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
  });

  it('debouncedPanCafeListRefresh - only calls addMarker once', (done) => {
    let spy = spyOn(component, 'clearMarkers');
    component.debouncedPanCafeListRefresh();
    component.debouncedPanCafeListRefresh();
    component.debouncedPanCafeListRefresh();
    setTimeout(() => {
      expect(component.clearMarkers).toHaveBeenCalledTimes(1);
      done();
    }, 2500)
  });



});
