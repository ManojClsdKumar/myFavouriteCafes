import { CafeItem } from './cafelist.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
declare var google: any;
import { google } from '@google/maps'
import * as ls from 'local-storage';

export interface CafeItem {
  'name': string;
  'location': { 'lat': number, 'lng': number }
  'favourite': boolean;
}



@Injectable({
  providedIn: 'root'
})
export class CafelistService {

  mapObject: google.maps.Map | null;
  placesService: google.maps.places.PlacesService | null;
  localCafeList: CafeItem[];
  localCafeListSubject: Subject<CafeItem[]> = new Subject();
  favouriteCafes: CafeItem[];

  constructor() {
    this.favouriteCafes = [];
    this.localCafeList = [];
    this.onLoad();
  }

  private setMapandService(mapObject: google.maps.Map) {
    this.mapObject = mapObject;
    this.placesService = new google.maps.places.PlacesService(mapObject);
  }

  refreshCafeList(mapObject: google.maps.Map) {
    return new Promise((resolve, reject) => {
      this.setMapandService(mapObject);

      const mapCenter: google.maps.LatLng = mapObject.getCenter();

      const request = {
        'location': mapCenter,
        'radius': 500,
        'query': 'cafe'
      };
      this.localCafeList = [];
      this.getCafes(request, resolve)
    })

  }

  getCafes(request, resolve) {
    this.placesService.textSearch(request, (results, status, pagination) => {
      if (status != google.maps.places.PlacesServiceStatus.OK) {
        throw ({ 'error': 'status returned non-OK: ' + status })
      }

      results.forEach((cafe) => {
        this.addCafeToList(cafe);
      })

      if (pagination.hasNextPage) {
        pagination.nextPage()
      } else {
        this.applyFavourites();
        this.saveCafes();
        resolve();
      }
    })
  }

  addCafeToList(cafe: any) {

    const cafeLat = cafe.geometry.location.lat()
    const cafeLng = cafe.geometry.location.lng();
    const LatLng = { 'lat': cafeLat, 'lng': cafeLng }

    let cafeObject = {
      'name': cafe.name,
      'location': LatLng,
      'favourite': false
    }

    this.localCafeList.push(cafeObject);
  }

  toggleFavouriteCafe(cafe: CafeItem) {

    let existingCafe = this.favouriteCafes.find((favCafe) => {
      return this.matchCafes(favCafe, cafe);
    });

    if (existingCafe) {
      this.favouriteCafes = this.favouriteCafes.filter((iteratingCafe) => {
        return !this.matchCafes(iteratingCafe, cafe);
      })

    } else {

      this.favouriteCafes.push(cafe);
    }

    this.applyFavourites();
    this.saveCafes();
  }

  applyFavourites() {
    this.localCafeList.forEach((cafe) => {

      let matchedcafe = this.favouriteCafes.find((favCafe) => {
        return this.matchCafes(favCafe, cafe)
      })

      if (matchedcafe) {
        cafe.favourite = true;
      } else {
        cafe.favourite = false;
      }
    });
  }


  matchCafes(favCafe, cafe) {
    if (favCafe.name == cafe.name
      && favCafe.location.lat == cafe.location.lat
      && favCafe.location.lng == cafe.location.lng) {
      return true;
    }
    return false;
  }

  saveCafes() {
    this.localCafeListSubject.next(this.localCafeList);
    ls('favlist', this.favouriteCafes);
  }

  onLoad() {
    let savedCafes = ls('favlist')
    if (savedCafes) {
      this.favouriteCafes = savedCafes;
    }
  }

}









